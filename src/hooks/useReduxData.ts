import { useSelector } from "react-redux";
import { RootState } from "store/root-reducer";

function useReduxData(name){
    return useSelector((state: RootState) => (state[name]));
} 

export default useReduxData;