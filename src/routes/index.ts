import { lazy } from 'react';
import { externalRoute } from './DistinctRoute/External';
import { internalRoute } from './DistinctRoute/Internal';

const Home = lazy(() => import("../core/Public/Home/Home"));
const Login = lazy(() => import("../core/Public/Login/Login"));
const Register = lazy(() => import("../core/Public/Register/Register"));
const Cart = lazy(() => import("../core/Public/Cart/Cart"));
const Checkout = lazy(() => import("../core/Public/Checkout/Checkout"));

const Boundary = lazy(() => import("../core/Protected/Boundary"));


export const appRoutes: CustomRoute[] = [
    {
        path: "/login",
        component: Login,
        type: "unauthorized"
    },
    {
        path: "/register",
        component: Register,
        type: "unauthorized"
    },
    {
        path: "/cart",
        component: Cart,
        type: "unauthorized"
    },
    {
        path: "/checkout",
        component: Checkout,
        type: "unauthorized"
    },
    {
        path: "/",
        component: Home,
        type: "unauthorized"
    },
    {
        path: "/",
        component: Boundary,
        children: [...internalRoute, ...externalRoute],
    }
]
