import { mainHost } from "./Links";
import Axios from "axios";

export function getData(link, onSuccess, onFail) {
    Axios.get(mainHost + link, {
        headers: {
            "content-type": "application/json",
        },
    })
        .then((res) => {
            onSuccess(res);
        })
        .catch((err) => {
            onFail(err);
        });
}

