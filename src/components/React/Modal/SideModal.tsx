import Cart from "core/Public/Cart/Cart";
import React from "react";

const SideModal = (props) => {
	return (
		<div
			className={`h-100 position-absolute ${
				props.display ? "" : "d-none"
			}`}
			style={{
				backgroundColor: "rgba(0, 0, 0, 0.2)",
				width: "100%",
				zIndex: 1,
			}}
		>
			<div className="row h-100">
				<div className="col-7 h-100" onClick={props.toggleSideBar} />
				<div
					className={`h-100 border col-5`}
					style={{
						right: 0,
						overflowY: "scroll",
						backgroundColor: "rgba(255, 255, 255, 1)",
					}}
				>
					<Cart toggleSideBar={props.toggleSideBar} />
				</div>
			</div>
		</div>
	);
};

export default SideModal;
