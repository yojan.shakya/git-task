import React, { useEffect } from "react";
import Logo from "assets/images/logo.png";
import "./navbar-theme.scss";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { useHistory } from "react-router-dom";
import { useState } from "react";
import { getLocalData } from "utils/localStorage/localStorageFunction";
import { getData } from "api/CommonApi";
import { readProducts } from "api/Links";
import { RootState } from "store/root-reducer";
import { connect, ConnectedProps, useSelector } from "react-redux";
import { getProducts } from "store/modules/products/getProducts";
import useReduxData from "hooks/useReduxData";

type cartProps = {
	id: number;
	name: string;
	image: string;
	price: string;
	stock: number;
	createDate: Date;
	category: any;
};

const NavBar = (props) => {
	const history = useHistory();
	// const [navItem, setNavItem] = useState<string[]>([]);

	// const { getProductsData, getProducts } = props;

	const cart = useReduxData("cart");

	// useEffect(() => {
	// 	getProducts();
	// 	if (getProductsData?.data?.product) {
	// 	}
	// }, []);

	// get nav item from the product
	// useEffect(() => {
	// 	if (getProductsData?.data?.product) {
	// 		let categoryArr: any = [];
	// 		let categoryObj: any = [];

	// 		getProductsData?.data?.product.forEach((item: cartProps) => {
	// 			if (!categoryArr.includes(item.category[1])) {
	// 				categoryArr.push(item.category[1]);
	// 				setNavItem(categoryArr);
	// 				let catObj = {
	// 					name: item.category[1],
	// 					value: [item.id],
	// 				};
	// 				categoryObj.push(catObj);
	// 			} else {
	// 				let labelName = item.category[1];
	// 				let newArray = categoryObj.map((itemCat) => {
	// 					if (itemCat.name === labelName) {
	// 						return {
	// 							name: itemCat.name,
	// 							value: [...itemCat.value, item.id],
	// 						};
	// 					}
	// 					return itemCat;
	// 				});
	// 				categoryObj = newArray;
	// 			}
	// 		});
	// 		console.log(categoryObj, "***---****");
	// 	}
	// }, [getProductsData]);

	return (
		<div className="px-5 navContainer">
			<div onClick={() => history.push("/")}>
				<img src={Logo} className="img-fluid imageStyle" alt="Logo" />
				<label className="companyName">Ecommerce</label>
			</div>
			{/* <div className="d-flex flex-row">
				{navItem?.map((item, index) => (
					<div key={index} className="mx-3">
						<label
							className="text-uppercase bold text-info pointer"
							onClick={() => console.log(item)}
						>
							{item}
						</label>
					</div>
				))}
			</div> */}
			<div className="pointer" onClick={props.toggleSideBar}>
				<AiOutlineShoppingCart />
				{cart.length > 0 && (
					<span className="badge badge-warning" id="lblCartCount">
						{cart.length}
					</span>
				)}
			</div>
		</div>
	);
};

// export default NavBar;

const mapStateToProps = (state: RootState) => ({
	getProductsData: state.products.getProducts,
});

const mapDispatchToProps = {
	getProducts,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

export default connector(NavBar);
