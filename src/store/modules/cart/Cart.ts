export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_FROM_CART = 'DELETE_FROM_CART';
export const UPDATE_CART = "UPDATE_CART";
export const RESET_CART = "RESET_CART";

export const addToCart = (product: any): DefaultAction => {
    return {
        type: 'ADD_TO_CART',
        payload: product

    }
}
export const updateToCart = (product: any): DefaultAction => {
    return {
        type: 'UPDATE_CART',
        payload: product
    }
}
export const deleteFromCart = (product: any): DefaultAction => {
    return {
        type: 'DELETE_FROM_CART',
        payload: product

    }
}
export const resetCart = (): DefaultAction => {
    return {
        type: 'RESET_CART',
        payload: []

    }
}

    
export type cartState = {
    id: number;
    name: string;
    price: string;
    image: string;
    stock: number;
    createDate: string;
    quantity: number;
    category: string[];
}[];

const cart: cartState = [];

const cartReducer = (state = cart, action: DefaultAction) => {
    const product = action.payload;
        switch (action.type) {
            case ADD_TO_CART:
                return [...state, action.payload];
            case DELETE_FROM_CART:
                // let deleted = state.filter((product) => product.id !== action.payload);
                let deleted =action.payload;
                 return [...deleted];
            case UPDATE_CART:
                let updatedCart = action.payload;
                return [...updatedCart]
            case RESET_CART:
                return action.payload;
            default:
                return state;

        }
    }

export default cartReducer;