import { combineReducers } from "redux";
import cartReducer from "./Cart";

const cartDataReducer = combineReducers({
    getCart: cartReducer,
});

export default cartDataReducer;