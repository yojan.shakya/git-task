import { apiList } from "store/actionNames";
import initDefaultAction from "store/helper/default-action";
import initDefaultReducer from "store/helper/default-reducer";
import initialState from "store/helper/default-state";

const obj = initialState;
let apiDetails = apiList.product.getProduct;

export default function getProductsReducer(store = {...obj}, action){
    let state = {...store};
    let actionName = apiDetails.actionName;
    
    return initDefaultReducer(actionName, action, state);
}

export const getProducts = () => async dispatch => {
    return initDefaultAction(apiDetails, dispatch, {disableSuccessToast: true});
}