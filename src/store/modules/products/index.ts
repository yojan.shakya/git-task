import { combineReducers } from "redux";
import getProductsReducer from "./getProducts";

const productReducer = combineReducers({
    getProducts: getProductsReducer,
});

export default productReducer;