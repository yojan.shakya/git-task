import React, { useState } from "react";
import { categoryProps } from "./Home";

const VerticleCategory = (props) => {
	const { category, handleClickFilter, active } = props;
	return (
		<div style={{ width: "10%" }} className="p-5">
			<h5
				className="text-uppercase pointer"
				onClick={() => handleClickFilter("all")}
			>
				Category
			</h5>
			{category &&
				category?.map((item: categoryProps, index: number) => (
					<div key={index}>
						<label
							className={`text-uppercase bold pointer ${
								active === item.name ? "text-info" : ""
							}`}
							onClick={() => handleClickFilter(item)}
						>
							{item.name}
						</label>
					</div>
				))}
		</div>
	);
};

export default VerticleCategory;
