import {
	FailToast,
	SuccessToast,
} from "components/React/ToastNotifier/ToastNotifier";
import useReduxData from "hooks/useReduxData";
import React from "react";
import { useDispatch } from "react-redux";
import { addToCart, updateToCart } from "store/modules/cart/Cart";
import { formatedDate } from "utils/utilsFunction/formatedDate";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";

const ProductCard = (props) => {
	const dispatch = useDispatch();
	const { item, filterItem } = props;
	const cart = useReduxData("cart");
	let amount = JSON.parse(item["price"].substring(1));

	const handleAddToCart = (product) => {
		let checkCart = cart?.filter((item) => item.id === product.id);

		if (checkCart.length <= 0) {
			let cartData = { ...product, quantity: 1 };
			SuccessToast("Successfully added to cart");
			dispatch(addToCart(cartData));
		} else {
			let updateCart = cart?.map((item) => {
				if (item.id === product.id) {
					if (item.stock === item.quantity) {
						let msg = `Cannot add to cart. ${item.name} reached max stock`;
						FailToast(msg);
						return item;
					} else {
						SuccessToast("Successfully added to cart");
						return { ...item, quantity: item.quantity + 1 };
					}
				}
				return item;
			});
			dispatch(updateToCart(updateCart));
		}
	};

	return (
		<div
			key={item.id}
			className={`border rounded m-2 ${
				filterItem === null
					? ""
					: filterItem?.includes(item.id)
					? ""
					: "d-none"
			}`}
		>
			<div>
				<img
					className="rounded"
					src={`https://electronic-ecommerce.herokuapp.com/${item.image}`}
					style={{
						height: "250px",
						width: "280px",
						objectFit: "contain",
					}}
					alt="product"
				/>
			</div>
			<div className="p-2 position-relative">
				<div className="d-flex flex-column">
					<h5 className="text-uppercase">{item.name}</h5>
					<label className="display-5">
						Price: Rs. {getCommaSeperateNumber(amount)}
					</label>
					<label>Stock: {item.stock}</label>
					<label>{formatedDate(item.createDate)}</label>
					<label className="text-capitalize">
						{item.category[1]}
					</label>
				</div>
				<div
					className="d-flex justify-content-center"
					style={{ position: "absolute", bottom: 15, right: 15 }}
				>
					<button
						className="rounded text-info"
						style={{
							backgroundColor: "#cfe7fa",
							borderColor: "#cfe7fa",
						}}
						onClick={() => handleAddToCart(item)}
					>
						Add To Cart
					</button>
				</div>
			</div>
		</div>
	);
};

export default ProductCard;
