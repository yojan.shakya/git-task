import React, { useEffect, useState } from "react";
import { connect, ConnectedProps, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { getProducts } from "store/modules/products/getProducts";
import { RootState } from "store/root-reducer";
import ProductCard from "./ProductCard";
import FilterComponent from "./FilterComponent";
import VerticleCategory from "./VerticleCategory";
import { resetCart } from "store/modules/cart/Cart";
import { SuccessToast } from "components/React/ToastNotifier/ToastNotifier";

export type categoryProps = {
	name: string;
	value: number[];
};
type cartProps = {
	id: number;
	name: string;
	image: string;
	price: string;
	stock: number;
	createDate: Date;
	category: any;
};

type CategoryOptionProps = {
	label: string;
	value: string;
};

const Home = (props) => {
	const history = useHistory();
	const dispatch = useDispatch();
	const { getProductsData, getProducts } = props;
	const [category, setCategory] = useState<categoryProps[] | null>(null);
	const [filterItem, setFilterItem] = useState<string[] | null>(null);
	const [categoryOption, setCategoryOption] = useState<
		CategoryOptionProps[] | null
	>(null);
	const [active, setActive] = useState("");
	const [filterParams, setFilterParams] = useState({
		category: null,
		date: null,
		price: null,
	});

	useEffect(() => {
		getProducts();
		if (history?.location?.state === "success") {
			window.history.replaceState(null, "");
			dispatch(resetCart());
			SuccessToast("Successfully Checkout");
		}
	}, []);

	// get nav item from the product
	useEffect(() => {
		if (getProductsData?.data?.product) {
			let categoryArr: any = [];
			let categoryObj: any = [];
			let categoryOpt: any = [
				{ label: "all category", value: "allcategory" },
			];

			getProductsData?.data?.product.forEach((item: cartProps) => {
				if (!categoryArr.includes(item.category[1])) {
					categoryArr.push(item.category[1]);
					categoryOpt = [
						...categoryOpt,
						{
							label: item.category[1],
							value: item.category[1],
						},
					];
					let catObj = {
						name: item.category[1],
						value: [item.id],
					};
					categoryObj.push(catObj);
				} else {
					let labelName = item.category[1];
					let newArray = categoryObj.map((itemCat) => {
						if (itemCat.name === labelName) {
							return {
								name: itemCat.name,
								value: [...itemCat.value, item.id],
							};
						}
						return itemCat;
					});
					categoryObj = newArray;
					setCategory(newArray);
				}
			});
			setCategoryOption(categoryOpt);
		}
	}, [getProductsData]);

	const handleClickFilter = (item) => {
		if (item === "all") {
			setActive("");
			setFilterItem(null);
		} else {
			setActive(item.name);
			setFilterItem(item.value);
		}
	};

	return (
		<div>
			<FilterComponent
				products={getProductsData?.data?.product}
				categoryOption={categoryOption && categoryOption}
				setFilterItem={setFilterItem}
				filterParams={filterParams}
				setFilterParams={setFilterParams}
			/>
			<div className="d-flex flex-row">
				<VerticleCategory
					category={category}
					handleClickFilter={handleClickFilter}
					active={active}
				/>
				{filterItem !== null && filterItem.length === 0 && (
					<label className="m-5">No Match</label>
				)}
				<div className="d-flex flex-wrap flex-row">
					{getProductsData?.data?.product?.map((item) => (
						<ProductCard item={item} filterItem={filterItem} />
					))}
				</div>
			</div>
		</div>
	);
};

// export default Home;
const mapStateToProps = (state: RootState) => ({
	getProductsData: state.products.getProducts,
});

const mapDispatchToProps = {
	getProducts,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof connector>;

export default connector(Home);
