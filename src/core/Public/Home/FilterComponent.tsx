import EnglishDatePicker from "components/React/EnglishDatepicker/EnglishDatepicker";
import StyledSelect from "components/React/StyledSelect/StyledSelect";
import React, { useEffect, useState } from "react";
import { formatedDate } from "utils/utilsFunction/formatedDate";
import { BiRefresh } from "react-icons/bi";

const FilterComponent = (props) => {
	const {
		products,
		categoryOption,
		setFilterItem,
		filterParams,
		setFilterParams,
	} = props;
	const [date, setDate] = useState<Date | null>(null);
	const [selectedCategory, setSelectedCategory] = useState({
		label: "All category",
		value: "all",
	});

	const handleAllFilter = (data) => {
		let filterParam = filterParams;
		filterParam = { ...filterParam, [data.name]: data.value.value };
		setFilterParams(filterParam);

		if (data.name === "category") {
			setSelectedCategory(data.value);
		}

		let filteredCategory =
			filterParam.category === null ||
			filterParam.category === "allcategory"
				? products
				: products?.filter(
						(item) => item.category[1] === filterParam.category
				  );
		let filteredDate =
			filterParam.date === null
				? filteredCategory
				: filteredCategory?.filter(
						(item) =>
							formatedDate(item.createDate) === filterParam.date
				  );
		let filteredPrice =
			filterParam.price === null || filterParam.price === ""
				? filteredDate
				: filteredDate?.filter(
						(item) =>
							JSON.parse(item["price"].substring(1)) ==
							filterParam.price
				  );
		let filteredId: string[] = [];
		filteredPrice?.forEach((item) => {
			filteredId.push(item.id);
		});
		setFilterItem(filteredId);
	};

	return (
		<div className="d-flex flex-row px-5 mt-5">
			{categoryOption && (
				<div className="w-25 mr-5">
					<StyledSelect
						name="category"
						placeholder="category"
						options={categoryOption}
						value={selectedCategory}
						onChange={(event) => handleAllFilter(event)}
						className="text-uppercase"
					/>
				</div>
			)}
			<div className="mr-5 d-flex flex-row">
				<EnglishDatePicker
					value={date && date}
					handleChange={(date) => {
						let data = {
							name: "date",
							value: { value: formatedDate(date) },
						};
						setDate(date);
						handleAllFilter(data);
					}}
				/>
				<div
					style={{ cursor: "pointer" }}
					onClick={() => {
						setDate(null);
						handleAllFilter({
							name: "date",
							value: { value: null },
						});
					}}
				>
					<BiRefresh size="30px" />
				</div>
			</div>
			<div>
				price:{" "}
				<input
					type="number"
					name="amount"
					placeholder="amount"
					onChange={(event) =>
						handleAllFilter({
							name: "price",
							value: { value: event.target.value },
						})
					}
				/>
			</div>
		</div>
	);
};

export default FilterComponent;
