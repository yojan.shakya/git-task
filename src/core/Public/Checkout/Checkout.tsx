import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import FormikValidationError from "components/React/FormikValidationError/FormikValidationError";
import CartCard from "../Cart/CartCard";
import { useHistory } from "react-router-dom";
import useReduxData from "hooks/useReduxData";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";

type cartProps = {
	id: number;
	name: string;
	image: string;
	price: string;
	stock: number;
	createDate: number;
	category: Date;
	quantity: number;
};

const initialValues = {
	name: "",
	billingAddress: "",
	deliveryAddress: "",
	contact: "",
	date: "",
};

const checkoutValidationSchema = Yup.object().shape({
	name: Yup.string().required("Name is required"),
	billingAddress: Yup.string().required("Billing address is required"),
	deliveryAddress: Yup.string().required("Delivery address is required"),
	contact: Yup.string().required("Contact is required"),
	date: Yup.string().required("Date is required"),
});

const Checkout = () => {
	const history = useHistory();
	const [userInfo, setUserInfo] = useState({});
	// const [cart, setCart] = useState<cartProps[] | null | undefined>(null);
	const [totalAmount, setTotalAmount] = useState(0);

	const cart = useReduxData("cart");

	const userDetails = [
		{
			id: "name",
			label: "Name",
			type: "text",
		},
		{
			id: "billingAddress",
			label: "Billing Address",
			type: "text",
		},
		{
			id: "deliveryAddress",
			label: "Delivery Address",
			type: "text",
		},
		{
			id: "contact",
			label: "Contact",
			type: "number",
		},
		{
			id: "date",
			label: "Date",
			type: "date",
		},
	];

	useEffect(() => {
		if (cart) {
			let total = 0;
			cart?.forEach((item) => {
				let amount = JSON.parse(item["price"].substring(1));
				total = total + item.quantity * amount;
			});
			setTotalAmount(total);
		}
	}, [cart]);

	const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
		setUserInfo({
			...userInfo,
			[event.currentTarget.name]: event.currentTarget.value,
		});
		setFieldValue(event.currentTarget.name, event.currentTarget.value);
	};

	// const handleSetQuantity = (
	// 	event: React.MouseEvent<HTMLButtonElement>,
	// 	id: number,
	// 	quantity: number
	// ) => {
	// 	let updatedData = cart?.map((item) => {
	// 		if (item.id === id) {
	// 			let newItem = {
	// 				...item,
	// 				quantity: quantity,
	// 			};
	// 			return newItem;
	// 		} else {
	// 			return item;
	// 		}
	// 	});
	// 	setCart(updatedData);
	// };

	const {
		values,
		errors,
		touched,
		setFieldValue,
		handleBlur,
		handleSubmit,
		resetForm,
	} = useFormik({
		enableReinitialize: true,
		initialValues: initialValues,
		validationSchema: checkoutValidationSchema,
		onSubmit: (values) => {
			console.log(values, "onSubmit");
			history.push({ pathname: "/", state: "success" });
			// alert("Checkout Successful");
		},
	});

	return (
		<div className="mx-5">
			<h2>User Details</h2>
			<form onSubmit={handleSubmit}>
				<div className="border rounded p-4 ">
					<div className="row">
						{userDetails.map((item, index) => {
							const { id, label, type } = item;
							let minDate;
							if (type === "date") {
								let todayDate = new Date();
								let day =
									todayDate.getDate() < 10
										? "0" + todayDate.getDate()
										: todayDate.getDate;
								let month =
									todayDate.getMonth() < 9
										? "0" + (todayDate.getMonth() + 1)
										: todayDate.getMonth() + 1;
								let year = todayDate.getFullYear();
								minDate = year + "-" + month + "-" + day;
								console.log(
									minDate,
									typeof todayDate.getMonth(),
									"min date"
								);
							}

							return (
								<div className="col-6" key={index}>
									<div className="w-100 pr-2">
										<label htmlFor={id}>{label}</label>
									</div>
									<div className="w-100 pr-2">
										{type === "date" ? (
											<input
												type={type}
												name={id}
												className="w-100"
												value={values[id]}
												placeholder={`Enter ${label}`}
												onChange={handleChange}
												onBlur={handleBlur}
												min={minDate}
											/>
										) : (
											<input
												type={type}
												name={id}
												className="w-100"
												value={values[id]}
												onBlur={handleBlur}
												placeholder={`Enter ${label}`}
												onChange={handleChange}
											/>
										)}
									</div>
									<FormikValidationError
										name={id}
										touched={touched}
										errors={errors}
									/>
								</div>
							);
						})}
					</div>
					<div>
						<h3>Payment Details</h3>
						<div>
							<div>Products: </div>
							{cart?.map((item) => {
								return (
									<CartCard
										item={item}
										// handleSetQuantity={handleSetQuantity}
										changeDisable={true}
									/>
								);
							})}
							<h5>
								Total: Rs. {getCommaSeperateNumber(totalAmount)}
							</h5>
						</div>
					</div>
					<div className="row justify-content-center mt-3">
						<button
							className="rounded text-info"
							style={{
								backgroundColor: "#cfe7fa",
								borderColor: "#cfe7fa",
								opacity: totalAmount === 0 ? 0.5 : 1,
							}}
							type="submit"
							disabled={totalAmount === 0 ? true : false}
						>
							Checkout
						</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default Checkout;
