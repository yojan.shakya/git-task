import React, { useEffect, useState } from "react";
import {
	AiOutlineMinus,
	AiOutlinePlus,
	AiOutlineCloseCircle,
} from "react-icons/ai";
import { useHistory } from "react-router-dom";
import { formatedDate } from "utils/utilsFunction/formatedDate";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";

const CartCard = (props) => {
	const history = useHistory();
	const { item, handleDelete, handleSetQuantity, changeDisable } = props;
	const [quantity, setQuantity] = useState(1);
	const [dateFormat, setDateFormat] = useState<string | null>(null);

	useEffect(() => {
		setQuantity(item.quantity);
		let dateFormated = formatedDate(item.createDate);
		setDateFormat(dateFormated);
	}, [item]);

	const handleQuantityChange = (
		event: React.MouseEvent<HTMLButtonElement>,
		stock: number,
		id: number
	) => {
		let count = 0;
		if (event.currentTarget.name === "decrement") {
			count = quantity === 1 ? 1 : quantity - 1;
		} else {
			count = quantity === stock ? stock : quantity + 1;
		}
		setQuantity(count);
		handleSetQuantity(event, id, count);
	};

	let amount = JSON.parse(item["price"].substring(1));

	return (
		<div
			className="border rounded mb-3"
			style={{ position: "relative" }}
			key={item.id}
		>
			<div className="d-flex flex-row ">
				<div style={{ height: "250px" }}>
					<img
						className="rounded img-fluid"
						src={`https://electronic-ecommerce.herokuapp.com/${item.image}`}
						style={{
							height: "250px",
							width: "250px",
							objectFit: "contain",
						}}
						alt="cart"
					/>
				</div>
				<div className="d-flex flex-column ml-2">
					<h4 className="text-uppercase">{item.name}</h4>
					<label className="display-5">
						Price: Rs. {getCommaSeperateNumber(amount)}
					</label>
					<label>Stock: {item.stock}</label>
					<label>{dateFormat}</label>
					<label className="text-capitalize">
						{item.category[1]}
					</label>
					<div className="d-flex align-items-center">
						<label>Quantity: </label>
						{changeDisable ? (
							<label>{quantity}</label>
						) : (
							<>
								<button
									className="mx-1 rounded d-flex align-items-center"
									name="decrement"
									onClick={(event) =>
										handleQuantityChange(
											event,
											item.stock,
											item.id
										)
									}
									style={{
										height: "20px",
										width: "20px",
									}}
									disabled={quantity === 1 ? true : false}
								>
									<AiOutlineMinus
										style={{ fontSize: "10px" }}
									/>
								</button>
								<div
									className="border mr-1 rounded p-1"
									style={{
										width: "20px",
										height: "20px",
										justifyContent: "center",
										display: "flex",
										alignItems: "center",
									}}
								>
									<label htmlFor="quantity">{quantity}</label>
								</div>
								<button
									className="rounded d-flex align-items-center mr-1"
									name="increament"
									onClick={(event) =>
										handleQuantityChange(
											event,
											item.stock,
											item.id
										)
									}
									disabled={
										quantity === item.stock ? true : false
									}
									style={{
										height: "20px",
										width: "20px",
									}}
								>
									<AiOutlinePlus
										style={{ fontSize: "10px" }}
									/>
								</button>
							</>
						)}
					</div>
					<label>
						Total: Rs. {getCommaSeperateNumber(amount * quantity)}
					</label>
				</div>
			</div>
			{!changeDisable && (
				<AiOutlineCloseCircle
					style={{
						position: "absolute",
						top: 10,
						right: 10,
						fontSize: "40",
						cursor: "pointer",
					}}
					id={item.id.toString()}
					onClick={handleDelete}
				/>
			)}
		</div>
	);
};

export default CartCard;
