import useReduxData from "hooks/useReduxData";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { deleteFromCart, updateToCart } from "store/modules/cart/Cart";
import { getCommaSeperateNumber } from "utils/utilsFunction/number-converter";
// import { RootState } from "store/root-reducer";
// import { setLocalData } from "utils/localStorage/localStorageFunction";
import CartCard from "./CartCard";

type cartProps = {
	id: number;
	name: string;
	image: string;
	price: string;
	stock: number;
	createDate: number;
	category: Date;
	quantity: number;
};

console.log("cart");
const Cart = (props) => {
	const history = useHistory();
	// const [cart, setCart] = useState<cartProps[] | null | undefined>(null);
	const dispatch = useDispatch();
	const cart = useReduxData("cart");
	const [totalAmount, setTotalAmount] = useState(0);
	useEffect(() => {
		if (cart) {
			let total = 0;
			cart?.forEach((item) => {
				let amount = JSON.parse(item["price"].substring(1));
				total = total + item.quantity * amount;
			});
			setTotalAmount(total);
		}
	}, [cart]);

	const handleSetQuantity = (
		event: React.MouseEvent<HTMLButtonElement>,
		id: number,
		quantity: number
	) => {
		let updatedData = cart?.map((item) => {
			if (item.id === id) {
				let newItem = {
					...item,
					quantity: quantity,
				};
				return newItem;
			} else {
				return item;
			}
		});
		dispatch(updateToCart(updatedData));
		// setCart(updatedData);
		// setLocalData("cart", JSON.stringify(updatedData));
	};

	// const cartsd = useSelector((state: RootState) => ({
	// 	cartData: state.cart,
	// }));
	// useEffect(() => {
	// 	console.log(cartsd, "redux cart *****   ");
	// }, [cart]);

	const handleDelete = (event: React.MouseEvent<SVGElement>) => {
		let updatedCart = cart?.filter(
			(item) => item.id.toString() !== event.currentTarget.id
		);
		// setCart(updatedCart);
		// setLocalData("cart", JSON.stringify(updatedCart));
		dispatch(deleteFromCart(updatedCart));
	};
	return (
		<div className="mx-5">
			<div className="d-flex justify-content-between my-3">
				<h2>Cart</h2>
				<button
					className="rounded text-info"
					style={{
						backgroundColor: "#cfe7fa",
						borderColor: "#cfe7fa",
						opacity: cart?.length < 1 ? 0.5 : 1,
					}}
					onClick={() => {
						props.toggleSideBar();
						history.push("/checkout");
					}}
					disabled={cart ? (cart?.length < 1 ? true : false) : true}
				>
					Checkout
				</button>
			</div>

			{cart && cart?.length !== 0 && (
				<h5>Total: Rs. {getCommaSeperateNumber(totalAmount)}</h5>
			)}
			{cart && cart?.length !== 0 ? (
				cart?.map((item) => {
					return (
						<CartCard
							item={item}
							handleDelete={handleDelete}
							handleSetQuantity={handleSetQuantity}
						/>
					);
				})
			) : (
				<label>No data</label>
			)}
		</div>
	);
};

export default Cart;
