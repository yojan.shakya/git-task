export const formatedDate = (rawDate) => {
    let createDate = new Date(rawDate);
    let day =
        createDate.getDate() < 10
            ? "0" + createDate.getDate()
            : createDate.getDate;
    let month =
        createDate.getMonth() < 9
            ? "0" + (createDate.getMonth() + 1)
            : createDate.getMonth() + 1;
    let year = createDate.getFullYear();
    let formatedDate = createDate.getDate() < 10 ? "0" + createDate.getDate() : createDate.getDate() + "-" + month + "-" + year;
    return formatedDate; 
}