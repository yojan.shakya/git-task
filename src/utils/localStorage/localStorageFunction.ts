// type getLocalDataProps = {
//     status: boolean,
//     message: string,
//     data: string
// }

// type GetLocalDataFunctionProps = (data: string) => getLocalDataProps;

export const getLocalData = (keyValue) => {
    let localData = localStorage.getItem(keyValue);
    let res = {
        status: false,
        message: "",
        data: ""
    }
    if (localData){
        res = {...res, status: true, message: "success", data: JSON.parse(localData)}
    } else {
        res = {...res, message: "error"}
    }
    return res;
}

export const setLocalData = (keyValue: string, data: string) => {
    return localStorage.setItem(keyValue, data);
}